import os, uuid, sys
from azure.storage.blob import BlockBlobService, PublicAccess


def uploadVideo(local_file_name ,  local_path):
	try:
        # Create the BlockBlockService that is used to call the Blob service for the storage account
        block_blob_service = BlockBlobService(account_name='rccwisedev', account_key='dylfRXtZPdUth1aSv/7c8lcxSc+T/Z9An95tYuQgw3koAQbkh3hHzriavq9clcnDDdWlwtmwrAxPm+IjeXqSNA==')

        # Create a container .
        container_name ='cwiseBlobs'
        block_blob_service.create_container(container_name)

        # Set the permission so the blobs are public.
        block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

        # Create a file in Documents to test the upload and download.
        full_path_to_file =os.path.join(local_path, local_file_name)

        

        print("Temp file = " + full_path_to_file)
        print("\nUploading to Blob storage as blob" + local_file_name)

        # Upload the created file, use local_file_name for the blob name
        block_blob_service.create_blob_from_path(container_name, local_file_name, full_path_to_file)

        return "https://rccwisedev.blob.core.windows.net/" + "container_name/" + local_file_name 

    except Exception as e:
        print(e)


def listVideo():
	try:
        # Create the BlockBlockService that is used to call the Blob service for the storage account
        block_blob_service = BlockBlobService(account_name='rccwisedev', account_key='dylfRXtZPdUth1aSv/7c8lcxSc+T/Z9An95tYuQgw3koAQbkh3hHzriavq9clcnDDdWlwtmwrAxPm+IjeXqSNA==')

        # Create a container called 'quickstartblobs'.
        container_name ='quickstartblobs'
        block_blob_service.create_container(container_name)

        # Set the permission so the blobs are public.
        block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

        # Create a file in Documents to test the upload and download.

        # List the blobs in the container
        print("\nList blobs in the container")
        generator = block_blob_service.list_blobs(container_name)
        for blob in generator:
            print("\t Blob name: " + blob.name)

    except Exception as e:
        print(e)


def downloadVideo(local_file_name ,  local_path):
	try:
        # Create the BlockBlockService that is used to call the Blob service for the storage account
        block_blob_service = BlockBlobService(account_name='rccwisedev', account_key='dylfRXtZPdUth1aSv/7c8lcxSc+T/Z9An95tYuQgw3koAQbkh3hHzriavq9clcnDDdWlwtmwrAxPm+IjeXqSNA==')

        # Create a container called 'quickstartblobs'.
        container_name ='quickstartblobs'
        block_blob_service.create_container(container_name)

        # Set the permission so the blobs are public.
        block_blob_service.set_container_acl(container_name, public_access=PublicAccess.Container)

        # Create a file in Documents to test the upload and download.

        full_path_to_file =os.path.join(local_path, local_file_name)

        
        # Download the blob(s).
        # Add '_DOWNLOADED' as prefix to '.txt' so you can see both files in Documents.
        full_path_to_file2 = os.path.join(local_path, str.replace(local_file_name ,'.mp4', '_DOWNLOADED.mp4'))
        print("\nDownloading blob to " + full_path_to_file2)
        block_blob_service.get_blob_to_path(container_name, local_file_name, full_path_to_file2)

        sys.stdout.write("Sample finished running. When you hit <any key>, the sample will be deleted and the sample "
                         "application will exit.")
        sys.stdout.flush()
        input()



    except Exception as e:
        print(e)

