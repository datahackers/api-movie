
from moviepy.editor import *
from PIL import Image 
from moviepy.video.fx import *


#redimensionnement d'image portrait paysage
def redimImage(image,dimension,name):

    basewidth=dimension
    img = Image.open(image)

    if (img.size[1]>=img.size[0]): 
        #portrait
        wpercent = (basewidth/float(img.size[0]))

        hsize = int((float(img.size[1])*float(wpercent)))
        img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    
    elif (img.size[1]<img.size[0]): 
        #paysage
        wpercent = (basewidth/float(img.size[1]))

        hsize = int((float(img.size[0])*float(wpercent)))
        img = img.resize((hsize,basewidth), Image.ANTIALIAS)
  
    img.save(name)

#redimImage("essai2.png",720,"essai/osssk.png")

#redimensionne une liste d'image
def redimList(liste,dimension):
    for i in range(0,len(liste)):
        redimImage(liste[i],dimension,"image"+i+"png")

#efface les fichier dans un dossier (pour chaques nouvelle appel)
def EraseFile(repertoire):
    import os

    files=os.listdir(repertoire)
    for i in range(0,len(files)):
        os.remove(repertoire+'/'+files[i])



import json
arr=[]
with open('try.json') as json_data:
    d = json.load(json_data)
    titre=d["title"]
    for i in range(0,7):
        arr.append(d["summary"][str(i)])
arr.insert(0,titre)

listeimage=["titre.jpg","1.jpg","2.jpg","3.jpg","4.jpg","5.jpg","6.jpg","7.jpg"]
#50 pour une ligne 
#permet de definir un height pour le pane
def heigtForText(text):
    nbLigne=text.splitlines()
    height=len(nbLigne)
    return height*40



    


#permet de definir le temps d'apparition de chaque phrase
def set_time(text):
    nb=len(text.split())
    time=nb*0.38
    return round(time)

#permet d'ajouter des saut de ligne en fonction du nombre de mots
def addNewline(text):
    word=text.split()
    nbwords=len(text.split())
    count_letters=0
    new_text=""
    nbword=0
    for i in word:
        count_letters+=len(i)
        nbword+=1

        if(count_letters>33):
            if(nbword>=nbwords):
                a=new_text+i
                return a

            new_text+=i+"\n"
            count_letters=0
        else:
            new_text+=i
            new_text+=" "
            
    return new_text


#print(addNewline("L’affaire Cambridge Analytica révèle aussi que Facebook n’a pas toujours le contrôle sur ces données (les vôtres et les miennes, en fait)."))
#print("okokokok"+str(heigtForText(addNewline("L’affaire Cambridge Analytica révèle aussi que Facebook n’a pas toujours le contrôle sur ces données (les vôtres et les miennes, en fait)."))))
#set_time("L’affaire Cambridge Analytica révèle aussi que\n Facebook n’a pas toujours le contrôle sur ces\n données (les vôtres et les miennes, en fait).")    

def constructVideo(listeImage,listeTexte,dimension):

    w = dimension
    h = dimension
    moviesize = w,h
    array = []
    duratotal=0

    for i in range(0,len(listeImage)):
        newText=addNewline(listeTexte[i])
        dura=set_time(newText)
        duratotal+=dura
        he=heigtForText(newText)
        if(he>180):
            newHeight=500-(he-180)
        else:
            newHeight=500
            
        redimImage("image/"+listeImage[i],dimension,"resize/resize"+str(i)+".png")
        
        clip = ImageClip('resize/resize'+str(i)   +'.png',duration=(dura))
        clip.fps=12

        if(i==0):
            txt = TextClip(newText, color='red',fontsize=35, font="Xolonium-Bold")
            txt2=txt.on_color(size=(720,he), color=(0, 0, 0), col_opacity=0.3)
            txt3=txt2.set_pos((0,newHeight))

        else:
            txt = TextClip(newText, color='white',fontsize=30, font="Xolonium-Bold")
            txt2=txt.on_color(size=(720,he), color=(0, 0, 0), col_opacity=0.7)
            txt3=txt2.set_pos((0,newHeight))

        title = (CompositeVideoClip([clip, txt3],size = moviesize))

        end=title.set_duration(dura)
        
        array.append(end)
    redimImage("image/logorc.png",70,"resize/logorc.png")
    logo=ImageClip('resize/logorc.png',duration=duratotal)
    logo=logo.set_pos((640,10))
    txtHack = TextClip("#DataHackers", color='white',fontsize=30, font="Xolonium-Italic")
    txtHack=txtHack.set_pos((510,690))
    txtHack = txtHack.set_duration(duratotal)




    video=concatenate_videoclips(array)
    video2=(CompositeVideoClip([video,logo,txtHack]))

    video2.write_videofile("myHolidays_edited.mp4",codec="libx264")
    EraseFile("resize/")



constructVideo(listeimage,arr,720)

